package com.hong.springboot.demo;

import com.hong.framework.redisplus.annotation.RedisLock;
import org.springframework.stereotype.Service;

/**
 * @author zengzh
 * @date create at 2018/5/16 16:01
 */
@Service
public class DemoService {

    @RedisLock(keys = "#id",expire = 30000,tryTimeout = 1000)
    public String testService(String id){
        return id;
    }
}
